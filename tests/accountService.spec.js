const chai = require('chai');
const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const AccountService = require('../src/services/accountService');

const { expect } = chai;

const mock = new MockAdapter(axios);

describe('AccountService', () => {

  beforeEach(() => {
    mock.onGet('https://api.blockcypher.com/v1/eth/main/addrs/738d145faabb1e00cf5a017588a9c0f998318012/balance')
      .reply(200, {
        address: '738d145faabb1e00cf5a017588a9c0f998318012',
        'total_received': 9762206505909057760,
        'total_sent': 9742951942909057760,
        balance: 19254563000000000,
        'unconfirmed_balance': 0,
        'final_balance': 19254563000000000,
        'n_tx': 704,
        'unconfirmed_n_tx': 0,
        'final_n_tx': 704,
        nonce: 414,
        'pool_nonce': 414
      });
  });

  describe('# getBalance()', () => {
    it('expect to return the balance converted from weis to eth', async () => {
      const result = await AccountService.getBalance('738d145faabb1e00cf5a017588a9c0f998318012');
      expect(result).to.be.eql({
        address: '738d145faabb1e00cf5a017588a9c0f998318012',
        'total_received': 9762206505909058000,
        'total_sent': 9742951942909057000,
        'unconfirmed_balance': 0,
        'final_balance': 19254563000000000,
        'n_tx': 704,
        'unconfirmed_n_tx': 0,
        'final_n_tx': 704,
        nonce: 414,
        'pool_nonce': 414,
        balance: 0.019254563
      });

    });
  });

  describe('# convertWeisToEth()', () => {
    it('expect to convert 19254563000000000 wei to 0.019254563 Eth', () => {
      expect(AccountService.convertWeisToEth(19254563000000000)).to.be.equal(0.019254563);
    });
  });
});
