const express = require('express');
const controller = require('../../controllers/account');

const router = express.Router();

router.route('/:accountId')
  .get(controller.get);

module.exports = router;
