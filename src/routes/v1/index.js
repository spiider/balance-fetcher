const express = require('express');
const account = require('./account');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (_req, res) => res.send('OK'));
router.use('/account', account);

module.exports = router;
