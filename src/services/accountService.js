const axios = require('axios');
const BigNumber = require('bignumber.js')

// TODO: change to a config file
const url = 'https://api.blockcypher.com/v1/eth/main/addrs/%%address%%/balance';

class AccountService {
  static async getBalance(accountId) {
    const reqUrl = url.replace('%%address%%', accountId)
    const response = await axios.get(reqUrl);
    const { balance, ...all } = response.data;
    const newBalance = this.convertWeisToEth(balance);
    return { ...all, balance: newBalance };
  }

  static convertWeisToEth(balance) {
    const from = new BigNumber(1, 10);
    const to = new BigNumber(1000000000000000000, 10);
    return Number(new BigNumber(balance, 10).mul(from).round(0, BigNumber.ROUND_DOWN).div(to).toString(10));
  }
}

module.exports = AccountService;
