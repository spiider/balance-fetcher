const httpStatus = require('http-status')
const AccountService = require('../services/accountService');

/**
 * Get user list
 * @public
 */
exports.get = async (req, res, next) => {
  const { accountId } = req.params;
  if (!accountId) {
    res.status(httpStatus.BAD_REQUEST);
    res.send('AccountId is required');
  }

  try {
    const result = await AccountService.getBalance(accountId)
    res.json(result);
  } catch (error) {
    next(error);
  }
};
